# CarCar

Team:

* Erika Linden - Service microservice
* Burcu Kantar Natoli - Sales microservice
* Everyone - Inventory microservices


## Design
![Img](/images/design.png)

## Project Beta - CarCar - How to Set up the project beta

CarCar is built using the following technologies:

Django
React
CSS
Bootstrap
JavaScript
Docker

(Note: Docker must be installed on your system.)

1- One person on the team fork repository from  https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta


2- The same person will invite the other member to the project-beta in the maintainer role.


3- Both members will clone the same repository.


4- Open terminal and type
   cd project-beta


5- Open code in VS code
   code .


6- Run commands below on your terminal


    docker volume create beta-data
    docker-compose build
    docker-compose up


7- Make sure your containers running on Docker and open http://localhost:3000 on your browser

## Service microservice

The Service Microservice has three models: AutomobileVO, Technician, and Appointment.

AutomobileVO interacts with the Inventory Microservice through the poller in the service/poll/poller.py file. The AutomobileVO was used to figure out whether a car's vin was sold by the dealership so a VIP status could be determined and populated on the front end on the Appointment List and Service History pages.

Technician is keeping track of employees that do service on vehicles. In order to create an appointment, a technician is needed.

Appointment is an entity that has a nested class to manage the status of the appointment instances and draws upon data from the Technician model in the microservice. The technician field is a reference point to the Technician model.


[*] Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser
| ----------------------------------------------------------------------------------- |


[*] ***Technicians APIs :***
| -------------------- |


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Technicians | GET | http://localhost:8080/api/technicians/
| Technician Detail | GET | http://localhost:8080/api/technicians/<int:id>/
| Create Technician | POST | http://localhost:8080/api/technicians/ |
| Delete Technician | DELETE | http://localhost:8080/api/technicians/<int:id>/


[*] JSON Body for Technician APIs :
| --------------------------------- |

* Create a Technician :
| -------------------- |
```
	{
		"first_name": "Charlie",
		"last_name": "Maximus",
		"employee_id": "7382049"
		}
```
**Please note: Employee ID's should be unique. You can not make two technicians with the same Employee ID.**


* Return value of creating and viewing a technician
| ---------------------------------------------- |
```
	{
		"href": "/api/technicians/1/",
		"first_name": "Charlie",
		"last_name": "Maximus",
		"employee_id": "7382049",
		"id": 1
	}
```
* Return value of getting a list of technicians
| -------------------------------------------- |
```
	{
		"technicians": [
			{
				"href": "/api/technicians/1/",
				"first_name": "Charlie",
				"last_name": "Maximus",
				"employee_id": 7382049,
				"id": 1
			}
		]
	}
```
* Return value of deleting technician by ID
| ----------------------------------------- |
```
	{
		"deleted": true
	}
```



[*] ***Appointment APIs :***
| ----------------------- |


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List Appointments | GET | http://localhost:8080/api/appointments/
| Appointment Detail | GET | http://localhost:8080/api/appointments/<int:id>/
| Create Appointment | POST | http://localhost:8080/api/appointments/
| Cancel Appointment | PUT | http://localhost:8080/api/appointments/<int:id>/cancel/ |
| Finish Appointment | PUT | http://localhost:8080/api/appointments/<int:id>/finish/
| Delete Appointment | DELETE | http://localhost:8080/api/appointments/<int:id>/

[*] JSON Body for Appointment APIs :
| --------------------------------- |

* Create Appointments :
| --------------------- |
```
	{
		"date_time": "2025-11-06",
		"reason": "Windshield Repair",
		"status": "Scheduled",
		"vin": "1GCEK19J38E136722",
		"customer": "Jeffrey Kabutcheek",
		"technician": 1
		}
```
**Please note: You need to make a technician before you can add one to the appointment.**


* Return value of getting a list of appointments
| ---------------------------------------------- |
```
	{
		"appointments": [
			{
				"id": 1,
				"customer": "Jeffrey Kabutcheek",
				"vin": "1GCEK19J38E136722",
				"technician": {
					"href": "/api/technicians/1/",
					"first_name": "Charlie",
					"last_name": "Maximus",
					"employee_id": 7382049,
					"id": 1
				},
				"reason": "Windshield repair",
				"date_time": "2025-11-06T10:33:00+00:00",
				"status": "Scheduled"
			}
		]
	}
```
* Return value of viewing appointment detail
| ------------------------------------------- |
```
{
	"id": 1,
	"customer": "Jeffrey Kabutcheek",
	"vin": "1GCEK19J38E136722",
	"technician": {
		"href": "/api/technicians/1/",
		"first_name": "Charlie",
		"last_name": "Maximus",
		"employee_id": 7382049,
		"id": 1
	},
	"reason": "Windshield repair",
	"date_time": "2025-11-06T10:33:00+00:00",
	"status": "Scheduled"
}
```
* Return value of changing the status of an appointment
| ------------------------------------------------------ |
**Finish**
```
	{
		"id": 1,
		"customer": "Jeffrey Kabutcheek",
		"vin": "1GCEK19J38E136722",
		"technician": {
			"href": "/api/technicians/1/",
			"first_name": "Charlie",
			"last_name": "Maximus",
			"employee_id": 7382049,
			"id": 1
		},
		"reason": "Windshield repair",
		"date_time": "2025-11-06T10:33:00+00:00",
		"status": "Finished"
	}
```
**Cancel**
```
	{
		"id": 1,
		"customer": "Jeffrey Kabutcheek",
		"vin": "1GCEK19J38E136722",
		"technician": {
			"href": "/api/technicians/1/",
			"first_name": "Charlie",
			"last_name": "Maximus",
			"employee_id": 7382049,
			"id": 1
		},
		"reason": "Windshield repair",
		"date_time": "2025-11-06T10:33:00+00:00",
		"status": "Finished"
	}
```
* Return value of deleting an appointment
| ---------------------------------------- |
```
{
	"deleted": true
}
```
## Sales microservice


The Sales Microservices contains four models AutomobileVO, Salesperson, Customer and Sales  on backend side of the project. In project CarCar used Django, Python as a backend code and React, Node.js used as a frontend applications. The AutomobileVo is designed as a value object that pulls data about the automobile from inventory microservice using a poller --> sale/poll/poller.py. The AutomobileVo used for pulling an information from automobile inventory which helped us to see is the vehicle still avaible for sale or already sold. The Sales Model used in Sales list, sales form, record of sales and salesperson history. The Customer Model used on customer list, adding a new customer to the system and also used showing the salesperson history on the page.


AUTOMOBILE SALES APIs :

[*] Salesperson APIs :

* List Salespeople - GET Request --> http://localhost:8090/api/salespeople/

JSON Body :
```
{
	"salesperson": [
		{
			"first_name": "John",
			"last_name": "Williams",
			"employee_id": "1",
			"id": 1
		},
		{
			"first_name": "Mary",
			"last_name": "Davis",
			"employee_id": "987654",
			"id": 5
		}
    ]
}
```
![Img](/images/listsalesperson.png)


* Create a Salesperson - POST Request --> http://localhost:8090/api/salespeople/

JSON Body :
```
{
	"first_name": "Camilla",
	"last_name": "Hung",
	"employee_id": 15
}
```
![Img](/images/createsalesperson.png)


* Delete a Specific Salesperson - DELETE Request --> http://localhost:8090/api/salespeople/:id/
```
{
	"message": "Does Not Exist"
}
```
![Img](/images/deletesalesperson.png)

[*] Customer APIs :

* List Customer - GET Request --> http://localhost:8090/api/customers/

JSON Body :
```
{
	"customers": [
		{
			"first_name": "Kelly",
			"last_name": "Miller",
			"address": "1400 Bush Street, San Francisco, CA",
			"phone_number": "4154151515",
			"id": 1
		},
		{
			"first_name": "Henry",
			"last_name": "Anderson",
			"address": "Lorton Ave, Burlingame, CA",
			"phone_number": "9128121212",
			"id": 6
		}
    ]
}
```
* Create a Customer - POST Request --> http://localhost:8090/api/customers/

JSON Body :
```
{
	"first_name": "Jane",
	"last_name": "Jones",
	"address": "45 Montgomery Street, San Francisco, CA ",
	"phone_number": 1234326787
}
```
* Delete a Customer - DELETE Request --> http://localhost:8090/api/customers/:id/
```
{
	"first_name": "Jim",
	"last_name": "Bob",
	"address": "New york",
	"phone_number": "9128121879",
	"id": null
}
```
[*] Sales APIs :

* List Sales - GET Request --> http://localhost:8090/api/sales/

JSON Body :
```
{
	"sales": [
		{
			"price": 1900,
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"sold": false,
				"import_href": "/api/automobiles/1C3CC5FB2AN120174/"
			},
			"salesperson": {
				"first_name": "John",
				"last_name": "Williams",
				"employee_id": "1",
				"id": 1
			},
			"customer": {
				"first_name": "Kelly",
				"last_name": "Miller",
				"address": "1400 Bush Street, San Francisco, CA",
				"phone_number": "4154151515",
				"id": 1
			}
		}
    ]
}
```





## Inventory microservices
