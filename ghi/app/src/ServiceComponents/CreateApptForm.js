import { useEffect, useState } from "react";

function CreateAppt() {
	const [technicians, setTechnicians] = useState([]);
	const [appointment, setAppointment] = useState({
		vin: "",
		customer: "",
		date_time: "",
		technician: "",
		reason: "",
	});

	const fetchTechnicians = async () => {
		const technicianUrl = "http://localhost:8080/api/technicians/";
		const response = await fetch(technicianUrl);

		if (response.ok) {
			const data = await response.json();
			setTechnicians(data.technicians);
		}
	};

	useEffect(() => {
		fetchTechnicians();
	}, []);

	const handleSubmit = async (e) => {
		e.preventDefault();

		const appointmentUrl = "http://localhost:8080/api/appointments/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(appointment),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(appointmentUrl, fetchConfig);

		if (response.ok) {
			setAppointment({
				vin: "",
				customer: "",
				date_time: "",
				technician: "",
				reason: "",
			});
		}
	};

	const handleFormChange = (e) => {
		const value = e.target.value;
		const inputName = e.target.name;
		setAppointment({
			...appointment,

			[inputName]: value,
		});
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a service appointment</h1>
					<form onSubmit={handleSubmit} id="create-appointment-form">
						<div className="form-floating mb-3">
							<input
								value={appointment.vin}
								onChange={handleFormChange}
								placeholder="vin"
								required
								type="text"
								name="vin"
								className="form-control"
							/>
							<label htmlFor="vin">Automobile VIN...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={appointment.customer}
								onChange={handleFormChange}
								placeholder="customer"
								required
								type="text"
								name="customer"
								className="form-control"
							/>
							<label htmlFor="customer">Customer name...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={appointment.date_time}
								onChange={handleFormChange}
								placeholder="date_time"
								required
								type="datetime-local"
								name="date_time"
								className="form-control"
							/>
							<label htmlFor="date_time">Date & Time...</label>
						</div>
						<div className="mb-3">
							<select
								value={appointment.technician}
								onChange={handleFormChange}
								name="technician"
								className="form-select"
								required
							>
								<option value="">Choose a technician...</option>
								{technicians.map((technician) => {
									return (
										<option
											value={technician.employee_id}
											key={technician.employee_id}
										>{`${technician.first_name} ${technician.last_name}`}</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								value={appointment.reason}
								onChange={handleFormChange}
								placeholder="reason"
								required
								type="text"
								name="reason"
								className="form-control"
							/>
							<label htmlFor="reason">Reason...</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default CreateAppt;
