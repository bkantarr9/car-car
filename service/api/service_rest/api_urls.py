from django.urls import path
from .views import api_technician, api_detail_technician, api_appointments, api_appt_detail, api_status_cancel, api_status_finish

urlpatterns = [
    path("technicians/", api_technician, name="api_technician"),
    path("technicians/<int:id>/", api_detail_technician, name="api_detail_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>/", api_appt_detail, name="api_appt_detail"),
    path("appointments/<int:id>/cancel/", api_status_cancel, name="api_status_cancel"),
    path("appointments/<int:id>/finish/", api_status_finish, name="api_status_finish"),
]
